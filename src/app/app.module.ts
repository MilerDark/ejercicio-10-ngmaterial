import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EndpointComponent } from './components/endpoint/endpoint.component';
import { MatTableModule} from '@angular/material/table';
// import { MatButtonModule , MatIconModule, MatInputModule, MatNativeDateModule} from '@angular/material';
import { ExporterService } from './services/exporter.service';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [
    AppComponent,
    EndpointComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatPaginatorModule,
    HttpClientModule,
    MatFormFieldModule
  
  ],
  providers: [ExporterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
