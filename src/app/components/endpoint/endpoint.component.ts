import { Component, OnInit } from '@angular/core';

import { ExporterService } from 'src/app/services/exporter.service';

import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

export interface PeriodicElement {
  nombre: string;
  id: number;
  edad: number;
  genero: string;
}



const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, nombre: 'Juan', edad: 23, genero: 'Masculino'},
  {id: 2, nombre: 'Matias', edad: 44, genero: 'Masculino'},
  {id: 3, nombre: 'Maria', edad: 21, genero: 'Femenino'},
  {id: 4, nombre: 'Luisa', edad: 24, genero: 'Femenino'},
  {id: 5, nombre: 'Rolando', edad: 41, genero: 'Masculino'},
  {id: 6, nombre: 'Messi', edad: 35, genero: 'Masculino'},
  {id: 7, nombre: 'Alex', edad: 33, genero: 'Masculino'},
  {id: 8, nombre: 'Mateo', edad: 11, genero: 'Masculino'},
  {id: 9, nombre: 'Patricia', edad: 65, genero: 'Femenino'},
  {id: 10, nombre: 'Noelia', edad: 22, genero: 'Femenino'},
];



@Component({
  selector: 'app-endpoint',
  templateUrl: './endpoint.component.html',
  styleUrls: ['./endpoint.component.css']
})
export class EndpointComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nombre', 'edad', 'genero'];
  dataSource = ELEMENT_DATA;
  clickedRows = new Set<PeriodicElement>();

  constructor(private excelService: ExporterService) { 
    // this.downloadPDF();
  }
  public exportAsPDF() {
    // Extraemos el
    const DATA: any = document.getElementById('htmlData');
    const doc = new jsPDF('p', 'pt', 'a4');
    const options = {
      background: 'white',
      scale: 3
    };
    html2canvas(DATA, options).then((canvas) => {

      const img = canvas.toDataURL('image/PNG');

      // Add image Canvas to PDF
      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
    });
  }

  

  ngOnInit(): void {
  }

  exportAsXLSX(): void{
    this.excelService.exportToExcel(this.dataSource, 'my export')
  }

}